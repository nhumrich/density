FROM python:alpine

RUN apk --no-cache add tini bash && pip install gunicorn
WORKDIR /app
COPY requirements.txt /app/
RUN pip install -r requirements.txt

COPY . /app/

CMD ["tini", "./startup.sh"]
