# Density - Nick Humrich

## Acccomplishments
- Can deploy to k8s using kube-deployment.yaml
- Application A publicly accessible over HTTPS
- Application B not accessible via the public internet
- Pod autoscaling based on CPU

## Delivery

You will find:

- A single Dockerfile (the apps are the same in this trivial example)
- kube-deployment file for deploying across k8s nodes
- A .gitlab-ci.yaml file for continuous delivery
- Horizontal pod autoscaler in the kube-deployment file

What you wont find because its out of scope:

- Handling an external database
- Using a database during testing (though this would be trivial)
- The actual creation of the ssl cert
- Docker images arent actually in dockerhub
- Handling secrets/authentication for actually pushing to dockerhub or deploying to a k8s cluster
