#!/bin/bash -e

if [ "$APP" == "B" ];
then
 exec gunicorn -w 4 -b 0.0.0.0:8000 app_b:application
else
 exec gunicorn -w 4 -b 0.0.0.0:8000 app_a:application
fi
